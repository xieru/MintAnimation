using MintAnimation.Core;
using UnityEditor;
using UnityEngine;

namespace MintAnimation.Editor
{
    [CustomPropertyDrawer(typeof(MintTweenOptions))]
    public class MintTweenOptionsEditor : PropertyDrawer
    {

        public float PropertyHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var width      = position.width;
            var labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = width * 0.25f;
            this.PropertyHeight         = 0;
            var titleRect = new Rect(position.position, new Vector2(width, 5));
            this.PropertyHeight += 5;
            var durationRect = new Rect(position.position + new Vector2(0, this.PropertyHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            this.PropertyHeight += EditorGUIUtility.singleLineHeight + 2;;
            var flipRect = new Rect(position.position + new Vector2(0,            this.PropertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            var backRect = new Rect(position.position + new Vector2(width * 0.5f, this.PropertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            this.PropertyHeight += EditorGUIUtility.singleLineHeight + 2;;
            var loopRect      = new Rect(position.position + new Vector2(0,            this.PropertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            var loopCountRect = new Rect(position.position + new Vector2(width * 0.5f, this.PropertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            this.PropertyHeight += EditorGUIUtility.singleLineHeight + 2;;
            var customEaseRect = new Rect(position.position + new Vector2(0,            this.PropertyHeight), new Vector2(width * 0.4f, EditorGUIUtility.singleLineHeight));
            var easeRect       = new Rect(position.position + new Vector2(width * 0.4f, this.PropertyHeight), new Vector2(width * 0.6f, EditorGUIUtility.singleLineHeight));
            this.PropertyHeight += EditorGUIUtility.singleLineHeight + 2;;
            var updaterTypeRect = new Rect(position.position + new Vector2(0, this.PropertyHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            this.PropertyHeight += EditorGUIUtility.singleLineHeight + 10;
            var Duration = property.FindPropertyRelative("Duration");
            var IsBack = property.FindPropertyRelative("IsBack");
            var IsFlip = property.FindPropertyRelative("IsFlip");
            var IsLoop = property.FindPropertyRelative("IsLoop");
            var LoopCount = property.FindPropertyRelative("LoopCount");
            var IsCustomEase = property.FindPropertyRelative("IsCustomEase");
            var EaseType = property.FindPropertyRelative("EaseType");
            var TimeCurve = property.FindPropertyRelative("TimeCurve");
            var UpdaterTypeEnum = property.FindPropertyRelative("UpdaterTypeEnum");
            
            // EditorGUI.DrawRect(new Rect(position.position , new Vector2(width , this.PropertyHeight)) , new Color(0.38f, 0.09f, 0.56f, 0.41f));
            EditorGUI.DrawRect(titleRect , Color.black);
            // EditorGUI.Slider(durationRect, Duration, 0.01f , 50f );
            EditorGUI.PropertyField(durationRect, Duration);
            EditorGUI.PropertyField(flipRect, IsFlip);
            EditorGUI.PropertyField(backRect, IsBack);
            EditorGUI.PropertyField(loopRect, IsLoop);
            if (IsLoop.boolValue)
            {
                EditorGUI.PropertyField(loopCountRect, LoopCount);
            }
            else
            {
                EditorGUI.LabelField(loopCountRect , "LoopCount" , "0");
            }
            EditorGUI.PropertyField(customEaseRect, IsCustomEase);
            if (IsCustomEase.boolValue)
            {
                EditorGUI.CurveField(easeRect, TimeCurve, Color.yellow, new Rect(0, 1, 0, 1));
            }
            else
            {
                EditorGUI.PropertyField(easeRect, EaseType);
            }
            EditorGUI.PropertyField(updaterTypeRect, UpdaterTypeEnum);
            EditorGUIUtility.labelWidth = labelWidth;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return this.PropertyHeight; }

    }
}