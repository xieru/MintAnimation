using System;
using System.Collections.Generic;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEditor;
using UnityEngine;

namespace MintAnimation.Editor
{
    [CustomPropertyDrawer(typeof(MintPlayerMix))]
    public class MintAnimationMixEditor : MintAnimationBaseEditor
    {

        public static Dictionary<MintPlayerType, Color> TypeColors = new Dictionary<MintPlayerType, Color>()
                                                                   {
                                                                       {MintPlayerType.None, Color.white},
                                                                       {MintPlayerType.Euler, Color.magenta},
                                                                       {MintPlayerType.GraphicColor, Color.blue},
                                                                       {MintPlayerType.Position, Color.cyan},
                                                                       {MintPlayerType.Rotation, Color.red},
                                                                       {MintPlayerType.Scale, Color.yellow},
                                                                       {MintPlayerType.AnchoredPos, Color.green},
                                                                       {MintPlayerType.GraphicAlpha, new Color(0.72f, 0f, 1f)},
                                                                       {MintPlayerType.SizeDelta, new Color(1f, 0.4f, 0f)},
                                                                   };

        protected override void DrawExtend(Rect position, SerializedProperty property)
        {
            var IsSync   = property.FindPropertyRelative("IsSync");
            var AnimType = property.FindPropertyRelative("PlayerType");

            var titleRect = new Rect(position.position,                                          new Vector2(position.width,         EditorGUIUtility.singleLineHeight));
            var typeRect  = new Rect(position.position,                                          new Vector2(position.width * 0.5f,  EditorGUIUtility.singleLineHeight));
            var asyncRect = new Rect(position.position + new Vector2(position.width * 0.75f, 0), new Vector2(position.width * 0.25f, EditorGUIUtility.singleLineHeight));
            AnimType.enumValueIndex = EditorGUI.Popup(typeRect, AnimType.enumValueIndex, AnimType.enumNames);
            IsSync.boolValue        = EditorGUI.Popup(asyncRect, IsSync.boolValue ? 1 : 0, new[] {"Wait", "Sync"}) == 1;
            EditorGUI.DrawRect(titleRect, TypeColors[(MintPlayerType) AnimType.enumValueIndex] * new Color(1, 1, 1, 0.3f));

            var curHeight  = titleRect.height;
            var width      = position.width;
            var labelWidth = EditorGUIUtility.labelWidth;
            var startRect  = new Rect(position.position + new Vector2(0, curHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            curHeight += startRect.height + 5;
            var endRect = new Rect(position.position + new Vector2(0, curHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            EditorGUIUtility.labelWidth = width * 0.25f;
            var startValue = property.FindPropertyRelative($"Start{this.GetValueName((MintPlayerType) AnimType.enumValueIndex)}");
            var endValue   = property.FindPropertyRelative($"End{this.GetValueName((MintPlayerType) AnimType.enumValueIndex)}");
            if (startValue != null)
            {
                if (((MintPlayerType) AnimType.enumValueIndex) == MintPlayerType.Rotation)
                {
                    startValue.vector4Value = this.DrawVector4(startRect, startValue.vector4Value, "StartValue");
                }
                else
                {
                    EditorGUI.PropertyField(startRect, startValue, new GUIContent("StartValue"));
                }
            }

            if (endValue != null)
            {
                if (((MintPlayerType) AnimType.enumValueIndex) == MintPlayerType.Rotation)
                {
                    endValue.vector4Value = this.DrawVector4(endRect, endValue.vector4Value, "EndValue");
                }
                else
                {
                    EditorGUI.PropertyField(endRect, endValue, new GUIContent("EndValue")); 
                }
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected string GetValueName(MintPlayerType playerType)
        {
            switch (playerType)
            {
                case MintPlayerType.None:
                    break;
                case MintPlayerType.Position:
                    return "V3";
                case MintPlayerType.Rotation:
                    return "V4";
                case MintPlayerType.Scale:
                    return "V3";
                case MintPlayerType.Euler:
                    return "V3";
                case MintPlayerType.GraphicColor:
                    return "Col";
                case MintPlayerType.AnchoredPos:
                    return "V3";
                case MintPlayerType.GraphicAlpha:
                    return "F";
                case MintPlayerType.SizeDelta:
                    return "V3";
            }

            return null;
        }

        protected Vector4 DrawVector4(Rect rect, Vector4 value, string label)
        {
            var labelRect = new Rect(rect.position, new Vector2(rect.width * 0.24f, rect.height));
            var x         = value.x;
            var y         = value.y;
            var z         = value.z;
            var w         = value.w;
            EditorGUI.PrefixLabel(labelRect, new GUIContent(label));
            var width = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = rect.width * 0.19f * 0.2f;
            x = EditorGUI.FloatField(new Rect(rect.position + new Vector2(rect.width * (0 * 0.19f + 0.24f), 0), new Vector2(rect.width * 0.19f, rect.height)), "x" ,x);
            y = EditorGUI.FloatField(new Rect(rect.position + new Vector2(rect.width * (1 * 0.19f + 0.24f), 0), new Vector2(rect.width * 0.19f, rect.height)), "y" ,y);
            z = EditorGUI.FloatField(new Rect(rect.position + new Vector2(rect.width * (2 * 0.19f + 0.24f), 0), new Vector2(rect.width * 0.19f, rect.height)), "z" ,z);
            w = EditorGUI.FloatField(new Rect(rect.position + new Vector2(rect.width * (3 * 0.19f + 0.24f), 0), new Vector2(rect.width * 0.19f, rect.height)), "w" ,w);
            EditorGUIUtility.labelWidth = width;
            return new Vector4(x, y, z, w);
        }

    }
}