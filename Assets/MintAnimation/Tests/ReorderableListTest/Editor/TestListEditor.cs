using System;
using System.Collections.Generic;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using EditorGUIUtility = UnityEditor.Experimental.Networking.PlayerConnection.EditorGUIUtility;

namespace MintAnimation.Tests.ReorderableListTest
{
    [CustomEditor(typeof(TestList))]
    public class TestListEditor : UnityEditor.Editor
    {
        private ReorderableList _reorderableList;

        private void OnEnable()
        {
            this._reorderableList = new ReorderableList(this.serializedObject , this.serializedObject.FindProperty("TTTList") , true , true , true , true);
            this._reorderableList.drawElementCallback = (rect, index, active, focused) =>
            {
                SerializedProperty serializedProperty = this._reorderableList.serializedProperty.GetArrayElementAtIndex(index);
                this._reorderableList.elementHeight = EditorGUI.GetPropertyHeight(serializedProperty);
                EditorGUI.PropertyField(rect, serializedProperty);
            };
            this._reorderableList.drawHeaderCallback = rect =>
            {
                GUI.Label(rect , "测试List");
            };
            this._reorderableList.onRemoveCallback = list =>
            {
                ReorderableList.defaultBehaviours.DoRemoveButton(list);
            };
            this._reorderableList.onAddCallback = list =>
            {
                if (list.serializedProperty != null)
                {
                    list.serializedProperty.arraySize++;
                    list.index = list.serializedProperty.arraySize - 1;
                }
                else
                {
                    ReorderableList.defaultBehaviours.DoAddButton(list);
                }
            };
        }

        private void OnClick(object userData)
        {
            var index = this._reorderableList.serializedProperty.arraySize;
            this._reorderableList.serializedProperty.arraySize++;
            this._reorderableList.index = index;
            var element = this._reorderableList.serializedProperty.GetArrayElementAtIndex(index);
            // var mintItem = (MintAnimItem)element.objectReferenceValue;
            // mintItem.Animation = new MintAnimEuler();
            this.serializedObject.ApplyModifiedProperties();
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            this.serializedObject.Update();
            this._reorderableList.DoLayoutList();
            this.serializedObject.ApplyModifiedProperties();
        }

    }
}