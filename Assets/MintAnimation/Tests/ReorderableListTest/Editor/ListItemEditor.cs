using System;
using UnityEditor;
using UnityEngine;

namespace MintAnimation.Tests.ReorderableListTest
{
    [CustomPropertyDrawer(typeof(ListItem))]
    public class ListItemEditor : PropertyDrawer 
    {

        private SerializedProperty Name;
        private SerializedProperty Table;
        private SerializedProperty IsColor;
        private SerializedProperty TColor;
        private float curHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            Name    = property.FindPropertyRelative("Name");
            Table   = property.FindPropertyRelative("Table");
            IsColor = property.FindPropertyRelative("IsColor");
            TColor  = property.FindPropertyRelative("Color");
            curHeight = 0;
            var width = position.width;
            var labelWidth = EditorGUIUtility.labelWidth;
            var titleRect = new Rect(position.position, new Vector2(position.width, 26));
            curHeight += 26;
            var nameRect = new Rect(new Vector2(position.x , position.y + curHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            var tableRect = new Rect(new Vector2(position.x + width * 0.5f, position.y + curHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            curHeight += EditorGUIUtility.singleLineHeight;
            var titleBreak = new GUIStyle(); 
            titleBreak.alignment = TextAnchor.MiddleCenter;
            titleBreak.fontSize = 18;
            EditorGUI.DrawRect(titleRect, new Color(0.09f, 0.29f, 0.42f));
            EditorGUI.DrawRect(nameRect, new Color(1f, 0.71f, 0.15f));
            EditorGUI.DrawRect(tableRect, new Color(0.39f, 0.54f, 1f));
            EditorGUI.LabelField(titleRect , new GUIContent($"<color=white>{property.type}</color>") , titleBreak);
            EditorGUIUtility.labelWidth = width * 0.25f;
            EditorGUI.PropertyField(nameRect, this.Name);
            EditorGUI.PropertyField(tableRect, this.Table);
            EditorGUIUtility.labelWidth = labelWidth;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) { return this.curHeight; }

    }
}