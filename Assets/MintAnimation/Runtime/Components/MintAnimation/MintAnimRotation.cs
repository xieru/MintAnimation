using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/Rotation")]
    public class MintAnimRotation : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerRotation MintPlayer;
        public override Core.MintAnim.MintPlayerBase     MintAnimation() { return this.MintPlayer; }
    }
}