using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/AnchoredPos")]
    [RequireComponent(typeof(RectTransform))]
    public class MintAnimAnchoredPos : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerAnchoredPos MintPlayer;
        public override Core.MintAnim.MintPlayerBase     MintAnimation() { return this.MintPlayer; }
    }
}