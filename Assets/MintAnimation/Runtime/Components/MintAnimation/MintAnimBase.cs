using System;
using MintAnimation.Core;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [Serializable]
    public abstract class MintAnimBase : MonoBehaviour , IMintAnim
    {

        public          bool         IsAutoPlay = true;
        protected       bool         _isInit;
        public abstract MintPlayerBase MintAnimation();
        private         MintTween    _tween;

        public void Init()
        {
            if (this._isInit)
                return;
            if (this.MintAnimation().Init(this.transform))
            {
                this._tween  = this.MintAnimation().Tween;
                this._isInit = true;
            }
        }

        public void StartAnim()
        {
            this.Init();
            this._tween.Start();
        }

        public void PauseAnim()
        {
            if (!this._isInit) return;
            this._tween.Pause();
        }

        public void ResumeAnim()
        {
            if (!this._isInit) return;
            this._tween.Resume();
        }

        public void StopAnim()
        {
            if (!this._isInit) return;
            this._tween.End();
        }

        private void OnEnable()
        {
            if (this.IsAutoPlay)
            {
                this.StartAnim();
            }
        }

        private void OnDisable()
        {
            this.PauseAnim();
        }

        private void OnDestroy()
        {
            if (this._isInit)
            {
                this._tween.Kill();
            }
        }

    }
}