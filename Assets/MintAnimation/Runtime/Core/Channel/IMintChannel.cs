using System;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace MintAnimation.Core
{
    public interface IMintChannel
    {

        void OnProcessUpdate(float   processValue);
        void UpdateStartValue(object startValue);
        void UpdateEndValue(object   endValue);

    }
}