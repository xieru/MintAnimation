using System;
using UnityEngine;

namespace MintAnimation.Core
{
    public class MintChannelColor : MintChannelBase<Color>, IMintChannel
    {


        public MintChannelColor(Action<Color> onProcess, Color startValue, Color endValue) : base(onProcess, startValue, endValue) { }
        public void OnProcessUpdate(float     processValue) { this.OnProcess?.Invoke(Color.Lerp(this.StartValue, this.EndValue, processValue)); }
        public void UpdateStartValue(object   startValue)   { this.StartValue = (Color) startValue; }
        public void UpdateEndValue(object     endValue)     { this.EndValue   = (Color) endValue; }

    }
}