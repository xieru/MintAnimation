using System;
using Unity.Mathematics;
using UnityEngine;

namespace MintAnimation.Core
{
    public class MintChannelVector3 : MintChannelBase<Vector3>, IMintChannel
    {

        public MintChannelVector3(Action<Vector3> onProcess, Vector3 startValue, Vector3 endValue) : base(onProcess, startValue, endValue) { }
        public void OnProcessUpdate(float         processValue) { this.OnProcess?.Invoke((this.EndValue - this.StartValue) * processValue + this.StartValue); }
        public void UpdateStartValue(object       startValue)   { this.StartValue = (Vector3) startValue; }
        public void UpdateEndValue(object         endValue)     { this.StartValue = (Vector3) endValue; }

    }
}