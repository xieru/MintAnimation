using System;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace MintAnimation.Core
{
    public abstract class MintChannelBase<T> where T : struct
    {

        protected T         StartValue;
        protected T         EndValue;
        public    Action<T> OnProcess { get; protected set; }

        protected MintChannelBase(Action<T> onProcess, T startValue, T endValue)
        {
            this.OnProcess  = onProcess;
            this.StartValue = startValue;
            this.EndValue   = endValue;
        }

    }
}