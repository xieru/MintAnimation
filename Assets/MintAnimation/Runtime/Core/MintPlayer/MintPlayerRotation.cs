using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerRotation : MintPlayerBase
    {

        public             Quaternion   StartValue;
        public             Quaternion   EndValue;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = new Quaternion(playerMix.StartV4.x , playerMix.StartV4.y , playerMix.StartV4.z , playerMix.StartV4.w );
            this.EndValue = new Quaternion(playerMix.EndV4.x , playerMix.EndV4.y , playerMix.EndV4.z , playerMix.EndV4.w );
        }

        protected override IMintChannel Channel() { return new MintChannelRotation(this.OnUpdate, this.StartValue, this.EndValue); }

        public override void OnAutoStartValue()
        {
            if (this.IsLocal)
            {
                this.StartValue = this._transform.localRotation;
            }
            else
            {
                this.StartValue = this._transform.rotation;
            }
        }

        private void OnUpdate(Quaternion obj)
        {
            if (this.IsLocal)
            {
                this._transform.localRotation = obj;
            }
            else
            {
                this._transform.rotation = obj;
            }
        }

    }
}