using System;
using MintAnimation.Core;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerNone : MintPlayerBase
    {

        protected override void SetMixData(MintPlayerMix playerMix)
        {
        }

        protected override IMintChannel Channel()           { return new MintChannelFloat(this.OnUpdate, 0, 1); }
        public override    void         OnAutoStartValue()  { }
        private            void         OnUpdate(float obj) { }

    }
}