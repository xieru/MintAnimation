using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerPosition : MintPlayerBase
    {
        public Vector3 StartValue;
        public Vector3 EndValue;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartV3;
            this.EndValue = playerMix.EndV3;
        }

        protected override IMintChannel Channel() { return new MintChannelVector3(this.OnUpdate, this.StartValue, this.EndValue); }

        public override void OnAutoStartValue()
        {
            if (this.IsLocal)
            {
                this.StartValue = this._transform.localPosition;
            }
            else
            {
                this.StartValue = this._transform.position;
            }
        }

        private void OnUpdate(Vector3 obj)
        {
            if (this.IsLocal)
            {
                this._transform.localPosition = obj;
            }
            else
            {
                this._transform.position = obj;
            }
        }
    }
}