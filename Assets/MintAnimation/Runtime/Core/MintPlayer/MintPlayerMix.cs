using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerMix
    {

        public Vector3          StartV3;
        public Vector3          EndV3;

        public Color            StartCol;
        public Color            EndCol;
        
        public float            StartF;
        public float            EndF;
        
        public Vector4          StartV4;
        public Vector4          EndV4;

        public MintTweenOptions TweenOptions;
        
        public bool             IsLocal;
        public bool             IsAutoStartValue;

        public bool             IsSync;
        public MintPlayerType     PlayerType;

        public MintPlayerBase Player { get; private set; }

        private Transform _transform;
        private MintPlayerType _curPlayerType;

        public Action OnComplete;

        public bool InitAnim(Transform transform)
        {
            this._transform = transform;
            switch (this.PlayerType)
            {
                case MintPlayerType.None:
                    this.Player = new MintPlayerNone();
                    break;
                case MintPlayerType.Position:
                    this.Player = new MintPlayerPosition();
                    break;
                case MintPlayerType.Rotation:
                    this.Player = new MintPlayerRotation();
                    break;
                case MintPlayerType.Scale:
                    this.Player = new MintPlayerScale();
                    break;
                case MintPlayerType.Euler:
                    this.Player = new MintPlayerEuler();
                    break;
                case MintPlayerType.GraphicColor:
                    this.Player = new MintPlayerGraphicColor();
                    break;
                case MintPlayerType.GraphicAlpha:
                    this.Player = new MintPlayerGraphicAlpha();
                    break;
                case MintPlayerType.AnchoredPos:
                    this.Player = new MintPlayerAnchoredPos();
                    break;
                case MintPlayerType.SizeDelta:
                    this.Player = new MintPlayerSizeDelta();
                    break;
            }
            this._curPlayerType = this.PlayerType;
            this.Player.UpdateForMix(this);
            var t = this.Player.Init(transform);
            if (t)
            {
                this.Player.Tween.OnComplete += this.OnCompleteAnim;
            }
            return t;
        }

        public void UpdateData()
        {
            if (this.PlayerType != this._curPlayerType)
            {
                this.Player.Tween?.Kill();
                this.InitAnim(this._transform);
            }
            else
            {
                this.Player.UpdateForMix(this);
            }
        }
        private void OnCompleteAnim()
        {
            this.OnComplete?.Invoke();
        }
    }
}