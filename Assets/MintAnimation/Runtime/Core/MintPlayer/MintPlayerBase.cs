using System;
using MintAnimation.Core;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public abstract class MintPlayerBase
    {
        protected Transform        _transform;
        public    MintTweenOptions TweenOptions;
        public    bool             IsLocal;
        public    bool             IsAutoStartValue;
        public    MintTween        Tween { get; protected set; }

        public MintPlayerBase UpdateForMix(MintPlayerMix playerMix)
        {
            this.IsLocal = playerMix.IsLocal;
            this.IsAutoStartValue = playerMix.IsAutoStartValue;
            this.TweenOptions = playerMix.TweenOptions;
            this.SetMixData(playerMix);
            return this;
        }

        protected abstract void SetMixData(MintPlayerMix playerMix);

        protected abstract IMintChannel Channel();

        public abstract void OnAutoStartValue();

        public virtual bool Init(Transform transform)
        {
            this._transform = transform;
            if (this.IsAutoStartValue)
                this.OnAutoStartValue();
            this.Tween = new MintTween(this.Channel(), this.TweenOptions);
            return this._transform != null;
        }

    }
}