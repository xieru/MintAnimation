using UnityEngine;

namespace MintAnimation.Core
{
    [System.Serializable]
    public class MintTweenOptions
    {
        public float                Duration = 0.35f;

        public bool                 IsBack;
        public bool                 IsFlip;
        public bool                 IsLoop;
        public int                  LoopCount = -1;
       
        public bool                 IsCustomEase = false;
        public MintEaseMethod       EaseType = MintEaseMethod.Linear;
        public AnimationCurve       TimeCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
        
        public UpdaterTypeEnum      UpdaterTypeEnum = UpdaterTypeEnum.Coroutine;
    }
}